class TopController < ApplicationController
    def main
        if session[:login_uid] == nil
            render template: 'top/login'
        else
            render template: 'top/main'
        end
    end
    
    def login
        if params[:uid] == "kindai" && params[:pass] == "sanriko"
            session[:login_uid] = params[:uid]
            redirect_to '/'
        else
             render template: "top/error"
        end
    end
end
